<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    public $timestamps = false;

    use SoftDeletes;

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id')->withTrashed();
    }
}
