<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getArticles()
    {
        $articles = Article::with('category', 'user')->paginate(10);

        return view('articles', ['articles' => $articles]);
    }

    public function newArticle()
    {
        $categories = Category::all();
        $users = User::all();

        return view('forms.article', ['categories' => $categories, 'users' => $users]);
    }

    public function saveArticle(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'user_id' => 'required',
            'description' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $article = new Article();

        $article->title = $request->title;
        $article->category_id = $request->category_id;
        $article->user_id = $request->user_id;
        $article->description = $request->description;

        $image = $request->file('image');

        $imageName = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/storage');
        $image->move($destinationPath, $imageName);

        $article->image = $imageName;

        $article->save();

        return redirect('/admin/articles');
    }

    public function editArticle(Request $request, $id)
    {
        $article = Article::find($id);
        $categories = Category::all();
        $users = User::all();

        $category = Category::withTrashed()->where('id', '=', $article->category_id)->get();
        $user = User::withTrashed()->where('id', '=', $article->user_id)->get();

        $categories = $categories->merge($category);
        $users = $users->merge($user);

        if ($request->method() == 'POST') {
            $request->validate([
                'title' => 'required',
                'category_id' => 'required',
                'user_id' => 'required',
                'description' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $article = Article::find($id);

            $article->title = $request->title;
            $article->category_id = $request->category_id;
            $article->user_id = $request->user_id;
            $article->description = $request->description;

            $image = $request->file('image');

            if ($image) {
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/storage');
                $image->move($destinationPath, $imageName);

                $article->image = $imageName;
            }

            $article->save();
            return redirect('/admin/articles');
        }

        return view(
            'forms.article',
            ['article' => $article, 'categories' => $categories, 'users' => $users]
        );
    }

    public function deleteArticle(Request $request, $id)
    {
        Article::destroy($id);

        return redirect('/admin/articles');
    }
}
