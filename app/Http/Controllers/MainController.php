<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function getCategories()
    {
        $categories = Category::with('parent')->get();

        return response()->json($categories);
    }

    public function getArticles()
    {
        $articles = Article::with('category', 'user')->get();

        return response()->json($articles);
    }
}
