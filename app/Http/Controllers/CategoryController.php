<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCategories()
    {
        $categories = Category::with('parent')->paginate(10);

        return view('categories', ['categories' => $categories]);
    }

    public function newCategory()
    {
        $categories = Category::all();

        return view('forms.category', ['categories' => $categories]);
    }

    public function saveCategory(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $category = new Category();

        $category->title = $request->title;
        $category->parent_id = $request->parent_id;

        $category->save();

        return redirect('/admin/categories');
    }

    public function editCategory(Request $request, $id)
    {
        $category = Category::find($id);
        $categories = Category::where('id', '!=', $id)->get();

        $parent = Category::withTrashed()->where('id', '=', $category->parent_id)->get();

        $categories = $categories->merge($parent);

        if ($request->method() == 'POST') {
            $category->title = $request->title;
            $category->parent_id = $request->parent_id;

            $category->save();
            return redirect('/admin/categories');
        }

        return view('forms.category', ['editedCategory' => $category, 'categories' => $categories]);
    }

    public function deleteCategory(Request $request, $id)
    {
        Category::destroy($id);

        return redirect('/admin/categories');
    }
}
