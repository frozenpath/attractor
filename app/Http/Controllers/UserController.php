<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUsers()
    {
        $users = User::paginate(10);

        return view('users', ['users' => $users]);
    }

    public function editUser(Request $request, $id)
    {
        $user = User::find($id);

        if ($request->method() == 'POST') {
            $request->validate([
                'email' => 'required|email',
                'name' => 'required'
            ]);

            $user->name = $request->name;
            $user->email = $request->email;

            if ($request->password) {
                $user->password = Hash::make($request->password);
            }

            $user->save();
            return redirect('/admin/users');
        }

        return view('forms.user', ['user' => $user]);
    }

    public function newUser()
    {
        return view('forms.user');
    }

    public function saveUser(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required',
            'password' => 'required',
        ]);

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        return redirect('/admin/users');
    }

    public function deleteUser(Request $request, $id)
    {
        User::destroy($id);

        return redirect('/admin/users');
    }
}
