@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <a href="{{ route('new_user') }}" class="btn btn-primary m-2">New user</a>
        </div>
        @if (count($users))
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Created at</th>
                    <th scope="col">Updated at</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{ route('edit_user', $user->id) }}">Edit</a>
                                    @if (Auth::user()->id != $user->id)
                                        <a class="dropdown-item" href="{{ route('delete_user', $user->id) }}"
                                           onclick="confirmDeleting({{ $user->id }});">
                                            Delete
                                        </a>

                                        <form id="delete-user-form-{{ $user->id }}" action="{{ route('delete_user', $user->id) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endif

                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <div class="alert alert-info">
                No created users
            </div>
        @endif

            {{ $users->links() }}
        </div>

    </div>
@endsection

<script>
    function confirmDeleting(id)
    {
        event.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-user-form-' + id).submit();
            }
        })
    }
</script>
