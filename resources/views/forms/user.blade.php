@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="m-2">
            @if (isset($user))
                <form action="{{ route('edit_user', $user->id) }}" method="POST">
            @else
                <form action="{{ route('save_user') }}" method="POST">
            @endif
                @csrf

                <div class="form-group">
                    <label for="user-name">User name</label>
                    <input name="name" type="text" class="form-control" id="user-name" @if (isset($user)) value="{{ $user->name }}" @endif placeholder="Name" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="user-email">Email address</label>
                    <input name="email" type="email" class="form-control" id="user-email" @if (isset($user)) value="{{ $user->email }}" @endif aria-describedby="emailHelp" placeholder="Email" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="user-password">Password</label>
                    <input name="password" type="password" class="form-control" id="user-password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>


        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection
