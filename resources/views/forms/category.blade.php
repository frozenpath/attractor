@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="m-2">
            @if (isset($editedCategory))
                <form action="{{ route('edit_category', $editedCategory->id) }}" method="POST">
            @else
                <form action="{{ route('save_category') }}" method="POST">
            @endif
                @csrf

                <div class="form-group">
                    <label for="category-title">Title</label>
                    <input @if (isset($editedCategory)) value="{{ $editedCategory->title }}" @endif name="title" type="text" class="form-control" id="category-title" placeholder="Title" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="category-parent">Parent Category</label>
                    <select name="parent_id" class="form-control" id="category-parent">
                        <option value="0">No parent category</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" @if (isset($editedCategory) && $editedCategory->parent_id == $category->id) selected @endif>{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection
