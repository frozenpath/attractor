@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="m-2">
            @if (isset($article))
                <form action="{{ route('edit_article', $article->id) }}" method="POST" enctype="multipart/form-data">
            @else
                <form action="{{ route('save_article') }}" method="POST" enctype="multipart/form-data">
            @endif
                @csrf

                <div class="form-group">
                    <label for="article-title">Title</label>
                    <input @if (isset($article)) value="{{ $article->title }}" @endif name="title" type="text" class="form-control" id="article-title" placeholder="Title" autocomplete="off">
                </div>

                <div class="form-group">
                    <label for="article-category">Category</label>
                    <select name="category_id" class="form-control" id="article-category">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" @if (isset($article) && $article->category_id == $category->id) selected @endif>{{ $category->title }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="article-user">User</label>
                    <select name="user_id" class="form-control" id="article-user">
                        @foreach ($users as $user)
                            <option value="{{ $user->id }}" @if (isset($article) && $article->user_id == $user->id) selected @endif>{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="article-description">Description</label>
                    <textarea name="description" class="form-control" id="article-description" rows="3">@if (isset($article)) {{ $article->description }} @endif</textarea>
                </div>

                <div class="form-group">
                    <label for="article-image">Image</label>
                    <input name="image" type="file" class="form-control-file" id="article-image">
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection
