@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <a href="{{ route('new_article') }}" class="btn btn-primary m-2">New article</a>
        </div>

        @if (count($articles))
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Category</th>
                        <th scope="col">User</th>
                        <th scope="col">Image</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($articles as $article)
                        <tr>
                            <td>{{ $article->id }}</td>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->category['title'] }}</td>
                            <td>{{ $article->user['name'] }}</td>
                            <td>
                                <a href="/storage/{{ $article->image }}" target="_blank">
                                    <img src="/storage/{{ $article->image }}" alt="image" class="image-preview img-thumbnail">
                                </a>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('edit_article', $article->id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ route('delete_article', $article->id) }}"
                                           onclick="confirmDeleting({{ $article->id }});">
                                            Delete
                                        </a>

                                        <form id="delete-article-form-{{ $article->id }}" action="{{ route('delete_article', $article->id) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-info">
                        No created articles
                    </div>
                @endif

                {{ $articles->links() }}
            </div>
    </div>
@endsection

<script>
    function confirmDeleting(id)
    {
        event.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-article-form-' + id).submit();
            }
        })
    }
</script>
