@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <a href="{{ route('new_category') }}" class="btn btn-primary m-2">New category</a>
        </div>
        @if (count($categories))
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Parent category</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->title }}</td>
                            <td>{{ $category->parent['title'] }}</td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ route('edit_category', $category->id) }}">Edit</a>
                                        <a class="dropdown-item" href="{{ route('delete_category', $category->id) }}"
                                           onclick="confirmDeleting({{ $category->id }});">
                                            Delete
                                        </a>

                                        <form id="delete-category-form-{{ $category->id }}" action="{{ route('delete_category', $category->id) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-info">
                        No created categories
                    </div>
                @endif

                {{ $categories->links() }}
            </div>
    </div>
@endsection

<script>
    function confirmDeleting(id)
    {
        event.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-category-form-' + id).submit();
            }
        })
    }
</script>
