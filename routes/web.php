<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin/users');
});

Route::get('/admin', function () {
    return redirect('/admin/users');
});

Route::get('/categories', 'MainController@getCategories')->name('categories');
Route::get('/articles', 'MainController@getArticles')->name('articles');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/admin/users', 'UserController@getUsers')->name('users_list');
Route::get('/admin/categories', 'CategoryController@getCategories')->name('categories_list');
Route::get('/admin/articles', 'ArticleController@getArticles')->name('articles_list');

Route::get('/admin/users/new', 'UserController@newUser')->name('new_user');
Route::post('/admin/users/new', 'UserController@saveUser')->name('save_user');

Route::get('/admin/users/edit/{id}', 'UserController@editUser')->name('edit_user');
Route::post('/admin/users/edit/{id}', 'UserController@editUser')->name('edit_user');
Route::post('/admin/users/delete/{id}', 'UserController@deleteUser')->name('delete_user');

Route::get('/admin/categories/new', 'CategoryController@newCategory')->name('new_category');
Route::post('/admin/categories/new', 'CategoryController@saveCategory')->name('save_category');

Route::get('/admin/categories/edit/{id}', 'CategoryController@editCategory')->name('edit_category');
Route::post('/admin/categories/edit/{id}', 'CategoryController@editCategory')->name('edit_category');
Route::post('/admin/categories/delete/{id}', 'CategoryController@deleteCategory')->name('delete_category');

Route::get('/admin/articles/new', 'ArticleController@newArticle')->name('new_article');
Route::post('/admin/articles/new', 'ArticleController@saveArticle')->name('save_article');

Route::get('/admin/articles/edit/{id}', 'ArticleController@editArticle')->name('edit_article');
Route::post('/admin/articles/edit/{id}', 'ArticleController@editArticle')->name('edit_article');
Route::post('/admin/articles/delete/{id}', 'ArticleController@deleteArticle')->name('delete_article');
