1. `git clone https://bitbucket.org/frozenpath/attractor.git`
2. run `composer install`
3. create database
4. rename .env.example file to .env
5. fill database access details in .env file
6. run `php artisan db:seed` command to create admin user
7. login: `admin@admin.admin`   
   password: `secret`
